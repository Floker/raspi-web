const { Server } = require('net');



const END = 'END';
const host = "0.0.0.0";

const connections = new Map();

const error = (message) => {
    console.error(message);
    process.exit(1);
}

const sendMessage = (message, origin) => {

    for (const socket of connections.keys())
    {
        if (socket !== origin){
            socket.write(message);
        }

    }

}

const listen = (port) => {
    const server = new Server();
    server.on("connection", (socket) => {
        const remoteSocket = `${socket.remoteAddress}:${socket.remotePort}`;
        console.log(`new connection from ${remoteSocket}`);
        socket.setEncoding('utf-8');

        socket.on("data", (message) => {
            if (!connections.has(socket)){
                if (message.substr(0,5) === 'name:')
                {
                console.log(`Username ${message} set for connection ${remoteSocket}`);
                connections.set(socket,message);
                }
                else
                {
                    console.log(`Message from conection: ${message}`);
                }
            }
            else if (message === END) {
                connections.delete(socket);
                socket.end();
            } else {
                const fullMessage = `[${connections.get(socket)}]: ${message}`;
                sendMessage(fullMessage,socket);
                console.log(`${remoteSocket} -> ${fullMessage}`);
            }
        });

        socket.on("close", () => {
            console.log(`Conection with ${remoteSocket} closed`);
        });
    });

    server.listen({ port, host}, () => {
        console.log(`listening on port ${port}`);
    });
    server.on("error", (err) => error(err.message));
}

const main = () => {
    if(process.argv.length !== 3){
        error(`Usage: node ${__filename} port`);
    }

    let port = process.argv[2];
    if(isNaN(port)){
        error(`Invidalid port ${port}`);
    }
    port = Number(port);

    listen(port);

}

if (require.main === module) {
    main();
}